package ui;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.logging.Logger;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.*;

public class BoardPage {
    private static final Logger logger = Logger.getLogger(BoardPage.class.getName());
    private final SelenideElement cardsWindow = Selenide.$x("//span[@class='list-card-title js-card-name']");
    private List<SelenideElement> lists = Selenide.$$x("//div[@class='list js-list-content']");
    private List<SelenideElement> checkItem = Selenide.$$x("//div[@class='checklist-item no-assignee no-due checklist-item-state-complete']");
    private SelenideElement coverCard = Selenide.$x("//a[@class='button-link js-card-cover-chooser'] | //a[@class='window-cover-menu-button js-card-cover-chooser']");

    @Step("Удостовериться, что карточка находится в колонке Done")
    public BoardPage cardInNameBoard(String namelist, String nameCard) {
        element(By.xpath("//div[@class='js-list list-wrapper']")).shouldBe(Condition.visible);
        SelenideElement board = lists.stream().filter(el -> el.isDisplayed() && el.getText().contains(namelist)).findFirst().orElseThrow(()-> new NoSuchElementException("Колонка не найдена"));
//        Assert.assertTrue(list.isDisplayed(), "Колонка " + namelist + " не найдена");
        Assert.assertTrue(board.getText().contains(nameCard), "карточка в клонке отсутствует " + nameCard);
        return this;
    }
    @Step("Удостовериться, все пункты чек-боксов выполнены")
    public BoardPage checkBox(String nameItem) {
        boolean checkItemIsDisplayed = checkItem.stream().anyMatch(item -> item.getText().contains(nameItem));
        Assert.assertTrue(checkItemIsDisplayed,"Чек итем "+nameItem+ " отсутсвует");
        return this;
    }

    @Step("Сменить фон доски на голубой")
    public void changeColorBoard(String color) {
        if (element(By.xpath("//span[text()='Show menu']")).isDisplayed()) {
            element(By.xpath("//span[text()='Show menu']")).shouldBe(Condition.exist).click();
        }
        element(By.xpath("//a[@class='board-menu-navigation-item-link js-change-background']")).shouldBe(Condition.exist).click();
        element(By.xpath("//div[@class='title' and contains(text(),'Colors')]")).shouldBe(Condition.exist).click();
        switch (color) {
            case ("Зеленый"):
                element(By.xpath("//div[@class='image' and (@style='background-color: rgb(81, 152, 57);')]")).click();
                logger.info("Выбран цвет " + color);
                break;
            case ("Красный"):
                element(By.xpath("//div[@class='image' and (@style='background-color: rgb(176, 70, 50);')]")).click();
                logger.info("Выбран цвет " + color);
                break;
            case ("Розовый"):
                element(By.xpath("//div[@class='image' and (@style='background-color: rgb(205, 90, 145);')]")).click();
                logger.info("Выбран цвет " + color);
                break;
            case ("Оранжевый"):
                element(By.xpath("//div[@class='image' and (@style='background-color: rgb(210, 144, 52);')]")).click();
                logger.info("Выбран цвет " + color);
                break;
            case ("Голубой"):
                element(By.xpath("//div[@class='image' and (@style='background-color: rgb(0, 174, 204);')]")).click();
                logger.info("Выбран цвет " + color);
                break;
            case ("Серый"):
                element(By.xpath("//div[@class='image' and (@style='background-color: rgb(131, 140, 145);')]")).click();
                logger.info("Выбран цвет " + color);
                break;
            case ("Салатовый"):
                element(By.xpath("//div[@class='image' and (@style='background-color: rgb(75, 191, 107);')]")).click();
                logger.info("Выбран цвет " + color);
                break;
            case ("Синий"):
                element(By.xpath("//div[@class='image' and (@style='background-color: rgb(0, 121, 191);')]")).click();
                logger.info("Выбран цвет " + color);
                break;
            default:
                throw new RuntimeException("Выбранный цвет отсутствует");
        }
    }
    @Step("Поставить обложку с голубым цветом")
    public BoardPage changeColorCard(String color) {
        element(By.xpath("//a[@class='button-link js-card-cover-chooser'] ")).shouldBe(Condition.exist).click();
        switch (color) {
            case ("Зеленый"):
                element(By.xpath("//button[@class='_31xT7xOqkxPLkw _1hFyzxe1-LRBw8']")).click();
                logger.info("Выбран цвет " + color);
                break;
            case ("Желтый"):
                element(By.xpath("//button[@class='_31xT7xOqkxPLkw _1kFCqdmLY2X1fa _3LtK8pTh0IP6jF']")).click();
                logger.info("Выбран цвет " + color);
                break;
            case ("Розовый"):
                element(By.xpath("//button[@class='_31xT7xOqkxPLkw _3LXcWy8pkF_N7j']")).click();
                logger.info("Выбран цвет " + color);
                break;
            case ("Оранжевый"):
                element(By.xpath("//button[@class='_31xT7xOqkxPLkw _3fbBEWI2O2NMNN']")).click();
                logger.info("Выбран цвет " + color);
                break;
            case ("Голубой"):
                element(By.xpath("//button[@class='_31xT7xOqkxPLkw _3PYjFb7GK6Y5Cy']")).click();
                logger.info("Выбран цвет " + color);
                break;
            case ("Сиреневый"):
                element(By.xpath("//button[@class='_31xT7xOqkxPLkw _3GdCpFdaWSN4H4 _3LtK8pTh0IP6jF']")).click();
                logger.info("Выбран цвет " + color);
                break;
            case ("Салатовый"):
                element(By.xpath("//button[@class='_31xT7xOqkxPLkw _3WED4RNr6dLCTr']")).click();
                logger.info("Выбран цвет " + color);
                break;
            case ("Синий"):
                element(By.xpath("//button[@class='_31xT7xOqkxPLkw _3Rilp7XwjrQrfV")).click();
                logger.info("Выбран цвет " + color);
                break;
            default:
               throw  new RuntimeException("Выбранный цвет отсутствует");

        }
        element(By.xpath("//a[@class='pop-over-header-close-btn icon-sm icon-close']")).shouldBe(Condition.exist).click();


        return this;
    }
    @Step("Открытие карточки")
    public BoardPage openCard(String card) {
        element(byText(card)).click();
        logger.info("Открыта карточка "+ card);
        return this;
    }
    @Step("Закрытие карточки")
    public BoardPage closeCard() {
        element(By.xpath("//a[@class='icon-md icon-close dialog-close-button js-close-window dialog-close-button-dark']")).click();
        return this;
    }

    public BoardPage clickDueDate() {
        element(By.xpath("//span[@class='card-detail-badge-due-date-complete-icon']")).shouldBe(Condition.exist).click();

        return this;
    }

}