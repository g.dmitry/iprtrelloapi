package hooks;

import api.apiobject.Board;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import io.qameta.allure.Attachment;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import utils.DataBase;

public class Hooks {
    public String nameBoard = "KanbanTool";

    @BeforeTest
    public void setUp() {
        System.out.println("START");
        DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
        desiredCapabilities.setBrowserName("chrome");
//        Configuration.startMaximized = true;
        Configuration.browserSize ="1920x1080";
        Configuration.pageLoadTimeout = 40000;
        Configuration.timeout = 15000;
        DataBase.connect();
//        DataBase.deleteOldTable();
//        DataBase.addTable();
//        DataBase.updateTable();

    }

    @AfterTest
    public void tearDown() {
        Board.deleteBoard(nameBoard);
        DataBase.disconnect();
    }

}
