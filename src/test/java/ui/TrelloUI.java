package ui;

import org.testng.annotations.Test;
import utils.Cryptor;
import utils.DataBase;

import static com.codeborne.selenide.Selenide.open;

public class TrelloUI {
    public String nameBoard = "KanbanTool";
    public String login = "glazurin.dmitry.test@mail.ru";

    @Test(priority = 2)
    public void testUi(){
        open("https://trello.com");

        new LoginPage().signUp().userInput(login, Cryptor.decrypt(DataBase.extract("password")));
        new MainPage().clickOnBoard(nameBoard);
        new BoardPage().cardInNameBoard("Done1","Карточка для изучения АПИ")
                .openCard("Карточка для изучения АПИ")
                .clickDueDate()
                .checkBox("pervii checkitem")
                .changeColorCard("Голубой")
                .closeCard()
                .changeColorBoard("Голубой");

    }
}
