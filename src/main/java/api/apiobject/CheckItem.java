package api.apiobject;

import api.Specification;
import api.endpoints.EndPoints;
import io.qameta.allure.Step;
import io.restassured.response.Response;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import static io.restassured.RestAssured.given;

public class CheckItem {
    private static final Logger logger = Logger.getLogger(CheckItem.class.getName());
    private final Map<String, String> idCheckItems = new HashMap<>();

    public Map<String, String> getIdCheckItem() {
        return idCheckItems;
    }

    @Step("Добавляем чекпоинт '{name}'")
    public void addCheckItem(String idCheckList, String name) {
        Response response = given().spec(Specification.getReqSpec())
                .pathParam("id", idCheckList)
                .queryParam("name", name)
                .when().log().uri()
                .post(EndPoints.CHECKITEM.getEndPoints())
                .then().log().all()
                .statusCode(200)
                .extract().response();
        String id = response.jsonPath().get("id");
        idCheckItems.put(name, id);
        idCheckItems.put(id,name);
        logger.info("Добавляем чекпоинт в checklist "+name);

    }

    @Step("Обновляем чекпоинт")
    public void updateCheckItem(String idCard, String idCheckItem, String state) {
        given().spec(Specification.getReqSpec())
                .pathParam("id", idCard)
                .pathParam("idCheckItem", idCheckItem)
                .queryParam("state", state)
                .when()
                .put(EndPoints.UPDATE_CHECKITEM.getEndPoints())
                .then()
                .statusCode(200);
        logger.info("Обновляем чекпоинт " +idCheckItems.get(idCheckItem));

    }

}
