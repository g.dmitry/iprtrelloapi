package api.apiobject;

import api.Specification;
import api.endpoints.EndPoints;
import io.qameta.allure.Step;
import io.restassured.response.Response;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import static io.restassured.RestAssured.given;

public class Board {

    private static final Logger logger = Logger.getLogger(Board.class.getName());
    private static final Map<String, String> nameIdBoard = new HashMap<>();
    private static String id;

    @Step("Удаляем доску '{nameBoard}'")
    public static void deleteBoard(String nameBoard) {
        given().spec(Specification.getReqSpec())
                .pathParam("id", nameIdBoard.get(nameBoard))
                .when().log().uri()
                .delete(EndPoints.DELETE_BOARD.getEndPoints())
                .then().log().status()
                .statusCode(200);
        logger.info("Удаляем доску " + nameBoard);

    }

    public String getIdBoard(String name) {
        return nameIdBoard.get(name);
    }

    @Step("Cоздаем доску '{name}'")
    public void createBoard(String name) {
        Response response = given().spec(Specification.getReqSpec())
                .queryParam("name", name)
                .when().log().uri()
                .post(EndPoints.CREATE_BOARD.getEndPoints());
        response
                .then().log().status()
                .statusCode(200);

        id = response.jsonPath().get("id");
        nameIdBoard.put(name, id);
        nameIdBoard.put(id, name);
        logger.info("Создаем доску " + name);
    }
}
