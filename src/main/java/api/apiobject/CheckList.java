package api.apiobject;

import api.Specification;
import api.endpoints.EndPoints;
import io.qameta.allure.Step;
import io.restassured.response.Response;

import java.util.logging.Logger;

import static io.restassured.RestAssured.given;

public class CheckList {
    private static final Logger logger = Logger.getLogger(CheckList.class.getName());
    private String id;

    public String getId() {
        return id;
    }

    @Step("Создание чеклиста")
    public void addCheckList(String idCard) {
        Response response = given().spec(Specification.getReqSpec())
                .pathParam("id", idCard)
                .when().log().uri()
                .post(EndPoints.CHECKLIST.getEndPoints())
                .then()
                .statusCode(200)
                .extract().response();
        id = response.jsonPath().get("id");

        logger.info("Создаем чеклист в карточке ");
    }
}
