package ui;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selectors.byName;
import static com.codeborne.selenide.Selenide.*;

public class LoginPage {


    private static String login = "glazurin.dmitry.test@mail.ru";
    private SelenideElement signUpButton = $x("//a[@href='/login']");
    private SelenideElement passwordField = $x("//input[@name='password']");
    private SelenideElement enterButton = $x("//button[@id='login-submit']");
    private SelenideElement userField = $x("//input[@id='user']");
    private SelenideElement enterButton1 = $x("//input[@id='login']");

    public LoginPage signUp() {
        signUpButton.shouldBe(Condition.exist).click();
        return this;
    }
    @Step("Вход")
    public LoginPage userInput(String login, String password) {
        userField.shouldBe(Condition.exist).sendKeys(login);
        enterButton1.shouldBe(Condition.exist).click();
        sleep(1000);
        element(byName("password")).setValue(password).pressEnter();
        return this;
    }

}
