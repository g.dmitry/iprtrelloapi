package api;

import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import utils.DataBase;

import java.util.logging.Logger;

public class Specification {
    public static Logger logger;
    private static RequestSpecification reqSpec;

    public static RequestSpecification getReqSpec() {
        return reqSpec;
    }

    public static void requestSpec() {
        reqSpec = new RequestSpecBuilder()
               .addFilter(new AllureRestAssured())
                .setBaseUri("https://api.trello.com")
                .addQueryParam("key", DataBase.extract("key"))
                .addQueryParam("token", DataBase.extract("token"))
                .setContentType(ContentType.JSON)
                .log(LogDetail.ALL)
                .build();
    }

}
