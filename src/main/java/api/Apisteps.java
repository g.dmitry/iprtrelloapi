package api;

import api.apiobject.*;

import java.util.HashMap;
import java.util.Map;

public class Apisteps {
    private final Board board = new Board();
    private final Cards cards = new Cards();
    private final Attachment attachment = new Attachment();
    private final CheckList checkList = new CheckList();
    private final CheckItem checkItem = new CheckItem();
    private final Description description = new Description();
    private ListWithCards list = new ListWithCards();

    public Apisteps createBoard(String name) {
        board.createBoard(name);
        return this;
    }

    public Apisteps createList(String nameList, String nameBoard) {
        list.createList(board.getIdBoard(nameBoard), nameList);
        return this;
    }

    public Apisteps createCard(String nameCard, String nameList) {
        cards.createCard(list.getIdList(nameList), nameCard);
        return this;
    }

    public Apisteps addAttachment(String nameCard, String url) {
        attachment.addAttachment(cards.getIdCard(nameCard), url);
        return this;
    }

    public Apisteps addDescription(String desc, String nameCard) {
        description.addDescription(cards.getIdCard(nameCard), desc);
        return this;
    }

    public Apisteps createDate(String nameCard) {
        cards.createDate(cards.getIdCard(nameCard));
        return this;
    }

    public Apisteps createChecklist(String nameCard) {
        checkList.addCheckList(cards.getIdCard(nameCard));
        return this;
    }

    public Apisteps createCheckItem(String name) {
        checkItem.addCheckItem(checkList.getId(), name);
        return this;
    }

    public Apisteps updateCheckItem(String checkItemName, String nameCard) {
        checkItem.updateCheckItem(cards.getIdCard(nameCard), checkItem.getIdCheckItem().get(checkItemName), "complete");
        return this;
    }

    public Apisteps moveCardsInList(String oldName, String newName, String boardName) {
        list.moveCards(list.getIdList(oldName), list.getIdList(newName), board.getIdBoard(boardName));
        return this;
    }

    public Apisteps archiveList(String nameList) {
        list.createArchive(list.getIdList(nameList), "true");
        return this;
    }


}
