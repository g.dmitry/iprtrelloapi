package ui;

import com.codeborne.selenide.Condition;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.*;

public class MainPage {
    @Step("Открытие доски")
    public void  clickOnBoard (String nameBoard) {
        element(byText(nameBoard)).shouldBe(Condition.visible).click();

    }

}