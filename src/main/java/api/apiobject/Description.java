package api.apiobject;

import api.Specification;
import api.endpoints.EndPoints;
import io.qameta.allure.Step;

import java.util.logging.Logger;

import static io.restassured.RestAssured.given;

public class Description {
    private static final Logger logger = Logger.getLogger(Description.class.getName());

    @Step("Добавляем описание  к карточке")
    public void addDescription(String idCard, String description) {
        given().spec(Specification.getReqSpec())
                .pathParam("id", idCard)
                .queryParam("desc", description)
                .when().log().uri()
                .put(EndPoints.UPDATE_CARD.getEndPoints())
                .then()
                .statusCode(200);
        logger.info("Добавляем описание к карточке ");
    }

}
