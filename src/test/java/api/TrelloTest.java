package api;


import hooks.Hooks;
import io.qameta.allure.Allure;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import ui.BoardPage;
import ui.LoginPage;
import ui.MainPage;
import utils.Cryptor;
import utils.DataBase;
import utils.Listener;

import java.util.ArrayList;

import static com.codeborne.selenide.Selenide.open;
@Listeners(Listener.class)
public class TrelloTest extends Hooks {
    public String nameBoard = "KanbanTool";
    public String nameList = "Backlog";
    public String nameList2= "Done1";
    public String nameCard = "Карточка для изучения АПИ";
    public String imageUrl = "https://a.radikal.ru/a19/2201/2e/7fd0b68e0457.jpg";
    public String login = "glazurin.dmitry.test@mail.ru";
    public String pass = "smG-9jh-9aD-xcS";
    public ArrayList<String> checkItems1 = new ArrayList<>();
//    public String login = DataBase.extract("login");
//    public String pass = DataBase.extract("password");

    @Test(priority = 1)
    public void testApi() {
        Allure.getLifecycle().updateTestCase(testResult -> testResult.setName("APITEST"));
        Specification.requestSpec();
        Apisteps trelloAPI = new Apisteps();
        trelloAPI
                .createBoard(nameBoard)
                .createList(nameList,nameBoard)
                .createCard(nameCard,nameList)
                .addAttachment(nameCard,imageUrl)
                .createDate(nameCard)
                .addDescription("Тут будет отмечаться прогресс обучения",nameCard)
                .createChecklist(nameCard)
                .createCheckItem("pervii checkitem")
                .createCheckItem("vtoroi checkitem")
                .updateCheckItem("pervii checkitem",nameCard)
                .createList(nameList2,nameBoard)
                .moveCardsInList(nameList,nameList2,nameBoard)
                .archiveList(nameList)
                .updateCheckItem("vtoroi checkitem",nameCard);

    }


    @Test(priority = 2)
    public void testUi(){
        Allure.getLifecycle().updateTestCase(testResult -> testResult.setName("UITEST"));
        open("https://trello.com");

        new LoginPage().signUp().userInput(login, Cryptor.decrypt(DataBase.extract("password")));
        new MainPage().clickOnBoard(nameBoard);
        new BoardPage().cardInNameBoard("Done1","Карточка для изучения АПИ")
                .openCard("Карточка для изучения АПИ")
                .clickDueDate()
                .checkBox("pervii checkitem")
                .changeColorCard("Голубой")
                .closeCard()
                .changeColorBoard("Голубой");

    }

}
