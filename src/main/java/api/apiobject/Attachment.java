package api.apiobject;

import api.Specification;
import api.endpoints.EndPoints;
import io.qameta.allure.Step;

import java.util.logging.Logger;

import static io.restassured.RestAssured.given;

public class Attachment {
    private static final Logger logger = Logger.getLogger(Attachment.class.getName());

    @Step("Прикрепляем картинку к карточке по URL '{url}'")
    public void addAttachment(String idCard, String url) {
        given().spec(Specification.getReqSpec())
                .pathParam("id", idCard)
                .queryParam("url", url)
                .when().log().uri()
                .post(EndPoints.CREATE_ATTACHMENT_ON_CARD.getEndPoints())
                .then().log().status()
                .statusCode(200);
        logger.info("Прикрепляем картинку к карточке по "+ url);

    }
}
