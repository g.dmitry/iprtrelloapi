package utils;

import java.sql.*;
import java.util.logging.Logger;

public class DataBase {
    private final static String user = "postgres";
    private final static String pass = "postgres";
    private final static String url = "jdbc:postgresql://localhost:18000/trello_test";
    private final static String url2 = "jdbc:postgresql://localhost:5432/trello_test";
    private final static String url1 = "jdbc:postgresql://localhost:5432/db1";
    private static final Logger logger = Logger.getLogger(DataBase.class.getName());
    private static final String table = "CREATE TABLE pg_test " +
            "(id integer NOT NULL, " +
            "account text NOT NULL, " +
            "login text, " +
            "password text, " +
            "key text, " +
            "token text, " +
            "PRIMARY KEY (id))";
    private static final String upTable = "INSERT  INTO pg_test VALUES (1 ,'Dmitry', 'glazurin.dmitry.test@mail.ru' ,'koLwj11SSB2X+SXQ7wjEew+haj+YDCdz' ,'225ddd18b3f4cffe8c22b9b0a6f674e7' ,'bd2c73a6cc78b6368bd1710a9d13951a6f1027799bbd4e5db55bc62c879605a0')";

    private static Connection connection;
    // CREATE DATABASE trello_test WITH OWNER = postgres ENCODING = 'UTF8' CONNECTION = -1;

    public static void connect() {
        try {
            connection = DriverManager.getConnection(url2, user, "14071995");
            logger.info("Connect DATABASE");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void disconnect() {
        try {
            connection.close();
            logger.info("Disconnect DATABASE");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static String extract(String value) {
        String result = null;
        try
                (PreparedStatement statement = connection.prepareStatement("SELECT * FROM pg_test")) {
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = resultSet.getString(value);
                logger.info(value + " = " + result);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static void addTable() {
        try {
            Statement statement = connection.createStatement();
            statement.execute(table);
            logger.info("Database has been created");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateTable() {
        try {
            Statement statement = connection.createStatement();
            logger.info("Database has been created");
            statement.executeUpdate(upTable);
            logger.info("add " + (upTable));

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void deleteOldTable() {
        try {
            Statement dropTable = connection.createStatement();
            dropTable.executeUpdate("DROP TABLE pg_test");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


}

