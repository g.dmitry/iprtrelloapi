package api.apiobject;

import api.Specification;
import api.endpoints.EndPoints;
import io.qameta.allure.Step;
import io.restassured.response.Response;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import static io.restassured.RestAssured.given;

public class ListWithCards {
    private static final Logger logger = Logger.getLogger(ListWithCards.class.getName());
    private String id;
    private Map<String, String> nameIdList = new HashMap<>();

    public String getId() {
        return id;
    }

    public String getIdList(String name) {
        return nameIdList.get(name);
    }

    @Step("Создаем колонку")
    public void createList(String idBoard, String name) {
        Response response = given()
                .spec(Specification.getReqSpec())
                .queryParam("name", name)
                .pathParam("id", idBoard)
                .when().log().uri()
                .post(EndPoints.LIST.getEndPoints())
                .then().log().status()
                .statusCode(200).extract().response();
        id = response.jsonPath().get("id");
        nameIdList.put(name, id);
        logger.info("Cоздаем колонку " + name);

    }

    @Step("Архивируем колонку")
    public void createArchive(String idList, String value) {
        given().spec(Specification.getReqSpec())
                .pathParam("id", idList)
                .queryParam("value", value)
                .when()
                .put(EndPoints.ARCHIVE_LIST.getEndPoints())
                .then().statusCode(200);
    }

    @Step("Перемещаем карточки из колонки Backlog  в колонку Done")
    public void moveCards(String idList, String idListNew, String idBoard) {
        given().spec(Specification.getReqSpec())
                .pathParam("id", idList)
                .queryParam("idBoard", idBoard)
                .queryParam("idList", idListNew)
                .when()
                .post(EndPoints.MOVE_CARD.getEndPoints())
                .then()
                .statusCode(200);
        logger.info("Перенос карточки из колонки Backlog в колонку Done");

    }
}