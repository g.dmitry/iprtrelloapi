package api.endpoints;

public enum EndPoints {
    CREATE_BOARD ("/1/boards/"),
    CREATE_A_NEW_CARD ("/1/cards"),
    CREATE_A_NEW_LISTS ("/1/lists"),
    CREATE_ATTACHMENT_ON_CARD ("/1/cards/{id}/attachments"),
    UPDATE_CARD ("/1/cards/{id}"),
    DELETE_BOARD("/1/boards/{id}"),
    CHECKLIST ("/1/cards/{id}/checklists"),
    CHECKITEM ("/1/checklists/{id}/checkItems"),
    UPDATE_CHECKITEM ("/1/cards/{id}/checkItem/{idCheckItem}"),
    MOVE_CARD ("/1/lists/{id}/moveAllCards"),
    ARCHIVE_LIST ("/1/lists/{id}/closed"),
    CREATE_STICKER ("/1/cards/{id}/stickers"),
    LIST ("/1/boards/{id}/lists");





    private final String endPoint;

   EndPoints(String endPoints) {
        this.endPoint = endPoints;
    }

    public String getEndPoints(){
        return endPoint;
    }

}
