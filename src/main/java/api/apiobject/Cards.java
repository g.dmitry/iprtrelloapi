package api.apiobject;

import api.Specification;
import api.endpoints.EndPoints;
import io.qameta.allure.Step;
import io.restassured.response.Response;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import static io.restassured.RestAssured.given;

public class Cards {
    private static final Logger logger = Logger.getLogger(Cards.class.getName());
    public Map<String, String> nameIdCard = new HashMap<>();

    public String getIdCard(String nameCard) {
        return nameIdCard.get(nameCard);
    }


    @Step("Создаем карточку '{name}'")
    public void createCard(String idList, String name) {
        Response response = given().spec(Specification.getReqSpec())
                .queryParam("name", name)
                .queryParam("idList", idList)
                .when().log().method().log().uri()
                .post(EndPoints.CREATE_A_NEW_CARD.getEndPoints())
                .then()
                .statusCode(200).log().status()
                .extract().response();
        String id = response.jsonPath().get("id");
        nameIdCard.put(name, id);
        nameIdCard.put(id, name);
        logger.info("Создаем доску " + name);

    }


    @Step("Устанавливаем срок выполнения карточки")
    public void createDate(String idCard) {
        Calendar calendar = new GregorianCalendar();
        calendar.add(Calendar.DATE, 1);
        given().spec(Specification.getReqSpec())
                .pathParam("id", idCard)
                .queryParam("due", calendar.getTime())
                .when().log().uri()
                .put(EndPoints.UPDATE_CARD.getEndPoints())
                .then().log().status()
                .statusCode(200);
        logger.info("Устанавливаем срок выполнения карточки " + nameIdCard.get(idCard));

    }

}
